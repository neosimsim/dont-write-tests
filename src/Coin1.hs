module Coin1
  ( Coin(..)
  , maxCoinValue
  , validCoin
  , add
  ) where

import           Test.QuickCheck

newtype Coin =
  Coin Int
  deriving (Eq, Show)

instance Arbitrary Coin where
  arbitrary = Coin <$> choose (0, maxCoinValue)

maxCoinValue :: Int
maxCoinValue = 100000

validCoin :: Coin -> Bool
validCoin (Coin n) = 0 <= n && n <= maxCoinValue

add :: Coin -> Coin -> Maybe Coin
add (Coin a) (Coin b) =
  if a + b < maxCoinValue
    then Just (Coin (a + b))
    else Nothing
