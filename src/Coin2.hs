module Coin2
  ( Coin(..)
  , maxCoinValue
  , validCoin
  , add
  ) where

import           Test.QuickCheck

newtype Coin =
  Coin Int
  deriving (Eq, Show)

instance Arbitrary Coin where
  arbitrary = do
    NonNegative n <- arbitrary
    Coin <$>
      oneof [return n, return (maxCoinValue - n), choose (0, maxCoinValue)]

maxCoinValue :: Int
maxCoinValue = 100000

validCoin :: Coin -> Bool
validCoin (Coin n) = 0 <= n && n <= maxCoinValue

add :: Coin -> Coin -> Maybe Coin
add (Coin a) (Coin b) =
  if a + b <= maxCoinValue -- here the bug is fixed. Change back to '<' and rerun test 08.
    then Just (Coin (a + b))
    else Nothing
