# don't write tests
… generate them.

This project is a collection of code examples as described by John Hughes in
the [talk "Building on developers"][talk].

In order to see and compare the evolution of the tests written, the different
implementations are stored in different files `test/Spec??.hs`. [test/Spec.hs](test/Spec.hs)
glues them together using [hspec][]. Each file contains comments on the
changes.

## QuickCheck
[QuickCheck][] is a testing library which tests your function against
properties.  Once you define the properties QuickCheck tests the properties
against random input.

This is a powerful technique to create tests and—by so—better software.
On the other hand it turns out to be quite hard to come up with good properties,
espacially if you're used to writing unit test.

But all hope is not lost. The [talk][] and also this project presents a
technique to write properties out of unit tests.

## Running
Run the tests using

	stack test

You can also run specifics tests using the match feature of hspec, e.g.

	stack test --ta '--match "/02/"'
	stack test --ta '--match "/02/add/error"'

See

	stack test --ta --help

for more options.

[talk]: https://www.youtube.com/watch?v=NcJOiQlzlXQ "Building on developers' intuitions"
[hspec]: hspec.github.io
[QuickCheck]: https://hackage.haskell.org/package/QuickCheck "QuickCheck: Automatic testing of Haskell programs"
