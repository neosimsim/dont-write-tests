-- We can replace classify by cover to specify a distribution for the
-- test case, e. g. here we say we want 5% of the test to be a boundary case.
--
-- Running this test will (probably) result in waring
--
--       Only 3% boundary, but expected 5%
--
module Spec10
  ( run
  ) where

import           Coin2
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = describe "add" . it "satisfies all properties" . property $ prop_Add

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  cover 5 (abs (n - maxCoinValue) < 3) "boundary" $
  cover 40 (n <= maxCoinValue) "normal" $
  cover 40 (n > maxCoinValue) "overflow" $
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)
    n = a + b
