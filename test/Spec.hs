import           Spec01
import           Spec02
import           Spec03
import           Spec04
import           Spec05
import           Spec06
import           Spec07
import           Spec08
import           Spec09
import           Spec10
import           Spec11
import           Test.Hspec

main :: IO ()
main =
  hspec $ do
    describe "01" Spec01.run
    describe "02" Spec02.run
    describe "03" Spec03.run
    describe "04" Spec04.run
    describe "05" Spec05.run
    describe "06" Spec06.run
    describe "07" Spec07.run
    describe "08" Spec08.run
    describe "09" Spec09.run
    describe "10" Spec10.run
    describe "11" Spec11.run
