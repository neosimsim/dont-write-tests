-- We start by creating simple unit tests for our test function add.
module Spec01
  ( run
  ) where

import           Coin1
import           Test.Hspec

run :: SpecWith ()
run =
  describe "add" $ do
    it "works within range" $ (Coin 2 `add` Coin 2) `shouldBe` Just (Coin 4)
    it "errors out of range" $
      (Coin maxCoinValue `add` Coin 1) `shouldBe` Nothing
