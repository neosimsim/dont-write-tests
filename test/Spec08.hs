-- In order to "reach" the boundary case we have to adopt the arbitrary for the
-- coin.
--
-- Once done, we can see that the boundary case is in fact reached
--
-- 08
--   add
--     satisfies all properties
--       +++ OK, passed 100 tests:
--       54% normal
--       41% overflow
--        5% boundary
--
-- and tests at the boundary in fact discover a bug at the boundary,
-- see the implemeation of add in Coin2.
module Spec08
  ( run
  ) where

import           Coin2
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = describe "add" $ it "satisfies all properties" $ property prop_Add

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  label (summarize (a + b)) $
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)

summarize :: Int -> String
summarize n
  | abs (n - maxCoinValue) < 3 = "boundary"
  | n <= maxCoinValue = "normal"
  | otherwise = "overflow"
