-- If we want to add a new "unit test" we now simple define a new label, e. g.
-- for the boundary, i. e. where the sum is close maxCoinValue.
--
-- But running this test you might realize, that the label "boundary" does not
-- appear in the rest report. This is due to the distribution of the coins
-- generated. It is highly unlikely that the some of two generated coins are
-- close to maxCoinValue.
module Spec07
  ( run
  ) where

import           Coin1
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = describe "add" $ it "satisfies all properties" $ property prop_Add

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  label (summarize (a + b)) $
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)

summarize :: Int -> String
summarize n
  | abs (n - maxCoinValue) < 3 = "boundary" -- this line is new
  | n <= maxCoinValue = "normal"
  | otherwise = "overflow"
