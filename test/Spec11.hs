-- You might wonder, whats the point in using cover if you just get a warning
-- which gets lots a 1e1000 lines test report.
--
-- As a final touch we introduce the function checkCoverage. Applying checkCoverage
-- to your property tells QuickCheck to run enough test until the specified coverage
-- is reached or QuickCheck is sure that the coverage can't be reached, in which case
-- the test will fail.
--
-- Using the checkCoverage you can still modify arbitrary without having to fear, that
-- some test cases might not get reached anymore.
module Spec11
  ( run
  ) where

import           Coin2
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run =
  describe "add" . it "satisfies all properties" . property $
  checkCoverage prop_Add -- add checkCoverage to enforce specified distributions

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  cover 4 (abs (n - maxCoinValue) < 3) "boundary" $ -- try to increase the coverage of the boundary case. Is the test still successful?
  cover 40 (n <= maxCoinValue) "normal" $
  cover 40 (n > maxCoinValue) "overflow" $
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)
    n = a + b
