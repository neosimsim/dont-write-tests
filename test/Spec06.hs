-- Now that Spec05 we has a property test to cover all unit test
-- one might argue, if the actually covers all unit test.
--
-- To do so QuickCheck comes with a little helper function label.
--
-- Runing this test should display the coverage of the labels
--
-- 06
--   add
--     satisfies all properties
--        +++ OK, passed 100 tests:
--        55% normal
--        45% overflow
--
module Spec06
  ( run
  ) where

import           Coin1
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = describe "add" $ it "satisfies all properties" $ property prop_Add

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  label (summarize (a + b)) $
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)

-- depending on the sum we label the test with either "normal" or "overflow".
summarize :: Int -> String
summarize n
  | n <= maxCoinValue = "normal"
  | otherwise = "overflow"
