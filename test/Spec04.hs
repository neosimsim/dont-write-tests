-- If you run the previous test
--
-- stack test --ta ' --match "/02/"'
--
-- you'll realize that alot of runs get discarded, due to the constraint (==>).
-- One way to improve this, is to adopt the generation funtion (arbitrary) to
-- only generate values used by the test.
--
-- We need a wrapper type for each test case, e. g. normal an overflow.
module Spec04
  ( run
  ) where

import           Coin1
import           Test.Hspec
import           Test.QuickCheck

-- Wrapper type for touples of coins which can successfully be added.
data Normal =
  Normal Coin
         Coin
  deriving (Show)

instance Arbitrary Normal where
  arbitrary = do
    Coin a <- arbitrary
    b <- choose (0, maxCoinValue - a)
    return $ Normal (Coin a) (Coin b)

-- Wrapper type for touples of coins which can't successfully be added.
data Overflow =
  Overflow Coin
           Coin
  deriving (Show)

instance Arbitrary Overflow where
  arbitrary = do
    Coin a <- arbitrary
    b <- choose (maxCoinValue - a + 1, maxCoinValue)
    return $ Overflow (Coin a) (Coin b)

run :: SpecWith ()
run = do
  describe "add" $ do
    it "works within range" $ property prop_Normal
    it "errors out of range" $ property prop_Overflow
  describe "arbitrary coin" $ it "should be valid" $ property prop_Valid
  describe "arbitrary normal" $ it "should be valid" $ property prop_ValidNormal
  describe "arbitrary overflow" $
    it "should be valid" $ property prop_ValidOverflow

prop_Normal :: Normal -> Property
prop_Normal (Normal (Coin a) (Coin b)) =
  Coin a `add` Coin b === Just (Coin (a + b))

prop_Overflow :: Overflow -> Property
prop_Overflow (Overflow (Coin a) (Coin b)) = Coin a `add` Coin b === Nothing

prop_Valid :: Coin -> Bool
prop_Valid (Coin c) = validCoin (Coin c)

-- We should also test the genration function for normal.
prop_ValidNormal :: Normal -> Bool
prop_ValidNormal (Normal (Coin c) (Coin c')) =
  validCoin (Coin c) && validCoin (Coin c')

-- We should also test the genration function for overflow.
prop_ValidOverflow :: Overflow -> Bool
prop_ValidOverflow (Overflow (Coin c) (Coin c')) =
  validCoin (Coin c) && validCoin (Coin c')
