module Spec03
  ( run
  ) where

import           Coin1
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = do
  describe "add" $ do
    it "works within range" $ property prop_Normal
    it "errors out of range" $ property prop_Overflow
  describe "arbirary coin" $ it "should be valid" $ property prop_Valid

prop_Normal :: Coin -> Coin -> Property
prop_Normal (Coin a) (Coin b) =
  a + b < maxCoinValue ==> Coin a `add` Coin b === Just (Coin (a + b))

prop_Overflow :: Coin -> Coin -> Property
prop_Overflow (Coin a) (Coin b) =
  a + b > maxCoinValue ==> Coin a `add` Coin b === Nothing

-- We should also test the generator function for random coins.
prop_Valid :: Coin -> Bool
prop_Valid (Coin c) = validCoin (Coin c)
