-- In the previous implementation it turns out that it is no feasable approach to
-- replace one unit test by one property (test).
--
-- Another aproach is to write a single proterty for all units tests.
--
-- "One property to rule them all"
module Spec05
  ( run
  ) where

import           Coin1
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = describe "add" $ it "satisfies all properties" $ property prop_Add

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)
