-- We translate the unit test to properies, each unit test gets its own property.
module Spec02
  ( run
  ) where

import           Coin1
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run =
  describe "add" $ do
    it "works within range" $ property prop_Normal
    it "errors out of range" $ property prop_Overflow

prop_Normal :: Coin -> Coin -> Property
prop_Normal (Coin a) (Coin b)
  -- we add contraints using (==>) to only test successful computations
 = a + b < maxCoinValue ==> Coin a `add` Coin b === Just (Coin (a + b))

prop_Overflow :: Coin -> Coin -> Property
prop_Overflow (Coin a) (Coin b)
  -- we add contraints using (==>) to only test failed computations
 = a + b > maxCoinValue ==> Coin a `add` Coin b === Nothing
