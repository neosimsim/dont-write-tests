-- QuickCheck comes with another helper function classify. We can rewrite the
-- test by merging summarize and the property. But there is more …
module Spec09
  ( run
  ) where

import           Coin2
import           Test.Hspec
import           Test.QuickCheck

run :: SpecWith ()
run = describe "add" $ it "satisfies all properties" $ property prop_Add

prop_Add :: Coin -> Coin -> Property
prop_Add (Coin a) (Coin b) =
  classify (abs (n - maxCoinValue) < 3) "boundary" $
  classify (n <= maxCoinValue) "normal" $
  classify (n > maxCoinValue) "overflow" $
  (Coin a `add` Coin b) ===
  if validCoin c
    then Just c
    else Nothing
  where
    c = Coin (a + b)
    n = a + b
